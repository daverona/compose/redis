# daverona/docker-compose/redis

## Quick Start

```bash
cp .env.example .env  
# edit .env
docker-compose up --detach
```

```bash
docker container exec redis redis-server --version
docker container exec redis redis-cli set hello world                                                                                                                                 10:14:31
docker container exec redis redis-cli get hello
```

## References

* Redis Documentation: [https://redis.io/documentation](https://redis.io/documentation)
* Redis Configuration: [https://redis.io/topics/config](https://redis.io/topics/config)
* Redis repository: [https://github.com/antirez/redis](https://github.com/antirez/redis)
* Redis registry: [https://hub.docker.com/\_/redis](https://hub.docker.com/_/redis)
